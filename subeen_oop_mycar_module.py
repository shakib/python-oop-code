# Standalone program
class Car:
    name = ""
    colour = ""
    
    def start(self):
        print("Starting the engine")



if __name__ == '__main__':  # if use as a module or call from another file
    # test Case
    my_car = Car()
    my_car.name = 'Corolla'
    print(my_car.name)
    my_car.start()
    
print('my name is',__name__)# for testing