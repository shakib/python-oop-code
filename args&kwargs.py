def print_recieved_data(num1, num2, *args, **kwargs):
    print("num1 = ", num1)
    print("num2 = ", num2)
    print("*args = ", args)
    print("**kwargs = ", kwargs)
 
# case 1
print_recieved_data(10, 20)
# Output:
num1 = 10
num2 = 20
*args = ()
**kwargs = {}
 
# case 2
print_recieved_data(10, 20, 30, 40, name="Anji", age=23)
# Output:
num1 = 10
num2 = 20
*args = (30, 40)
**kwargs = {"name": "Anji", "age": 23}
 
def f1(a, b, c, d):
    return a+b+c+d
def f2(*args, **kwargs):
    return f1(*args, **kwargs)
 
# case3
sum = f2(1,3,4, d=100)
print sum
# Output: 108
'''
From the above code we can observe the follwing.

From case1

default values for *args are empty tuple and for **kwargs are empty dictionary. 
*args and **kwargs are not mandatory
we can use tuple functionalities on "args" and dict functinalities on "kwargs".

From case2

All extra arguments are stored in "args" in a tuple format.
All key, value arguments are stored in "kwargs" in a dict format.
From case3

when we use *args, **kwargs in function "f1(*args, **kwargs)" arguments are unpacked from the tuple and dictionary
f1(*args, **kwargs) = "f1(1,3,4, d=100)"
Use case:

used when number of parameters in a function are unknown

'''
class Parent(object):
    def __init__(self, a, b):
        # do something
        print(a, b)
class Child(Parent):
    def __init__(self, *args, **kwargs):
        # do something
        super(Parent, self).__init__(*args, **kwargs)
        # do something
