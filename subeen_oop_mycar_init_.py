class Car:
    name = ""
    colour = ""
    
    def __init__(self,name,colour):       #constructor / initializer
        self.name =name
        self.colour = colour
    
    def start(self):
        print("Starting the engine")
        
my_car = Car('Nano',"yellow")

print(my_car.name)
print(my_car.colour)
my_car.start()