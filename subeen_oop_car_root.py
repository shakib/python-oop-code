class Car:
    name = ""         # Data attribute
    colour = ""
    
    def start():      #  method
        print("Starting the engine")


Car.name = 'Alion'       # Accessing  Data attribute
Car.colour = 'black'
print("Name of the Car:", Car.name)
print("Colour:", Car.colour)
Car.start()               # Applying method
