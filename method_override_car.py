class Vehicle:
    """Base class for all vehicles and this quotations are called Docstring"""
    
    def __init__(self, Name, Manufacture, Color):
        self.name = Name
        self.manufacture = Manufacture
        self.color = Color

    def turn(self, direction):
        print("Turning", self.name, "to", direction)


class car(Vehicle):  # ------------------ INHERITENCE ---------------------
    """Car class constructor """
    
    def __init__(self, name, manufacturer, color, year):
        self.name = name
        self.manufacture = manufacturer
        self.color = color
        self.year = year
        self.wheels = 4
   
    
    def change_gear(self, gear_name):
        """Method for changing gear"""
        print(self.name, "is changing gear to", gear_name)

    def turn(self, direction):
        print("Turning from", self.name, "to", direction)
    
if __name__ == "__main__":
    c = car("Lancer Ex", "Mitsubishi", "Black",2017)
    v2 = Vehicle("Prado", "Toyota", "black")
    c.turn("right") # using child-class method means overriding parent class method
    v2.turn("right")