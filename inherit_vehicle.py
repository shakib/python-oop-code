class Vehicle:
    """Base class for all vehicles and this quotations are called Docstring"""
    
    def __init__(self,Name,Manufacture,Color):
        self.name = Name
        self.manufacture = Manufacture
        self.color = Color
        
    def drive(self):
        print("Driving",self.manufacture,self.name)
        
    def turn(self,direction):
        print("Turning",self.name,"to",direction)
        
    def brake(self):
        print(self.name,"is stopping")

if __name__ == "__main__":
    v1 = Vehicle("Lancer ex","Mitsubishi","red")
    v2 =Vehicle("Prado","Toyota","black")
    v3 = Vehicle("Nano","tata","blue")
    
    v1.drive()
    v2.drive()
    v3.drive()
    
    v1.turn("right")
    v2.turn("left")
    
    v1.brake()
    v2.brake()
    v3.brake()
    
class car(Vehicle):#------------------ INHERITENCE ---------------------
    """Car class"""
    def change_gear(self,gear_name):
        """Method for changing gear"""
        print(self.name,"is changing gear to", gear_name)
        
if __name__ == "__main__":
    c = car("LAncer Ex","Mitsubishi","Black")
    c.drive()
    c.brake()
    c.change_gear("p")
    