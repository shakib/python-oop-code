class Vehicle:
    
    """Base class for all vehicles and this quotations are called Docstring"""
    def __init__(self, Name, Manufacture, Color):
        self.name = Name
        self.manufacture = Manufacture
        self.color = Color


class car(Vehicle):  # ------------------ INHERITENCE ---------------------
    """Car class constructor """
    
    def __init__(self, name, manufacturer, color, year):
        print("Creating a car ")
        super().__init__(name,manufacturer,color)
        self.year = year
        self.wheels = 4

if __name__ == "__main__":
    c = car("Lancer Ex", "Mitsubishi", "Black",2017)
    print(c.name,c.year,c.wheels)
    

