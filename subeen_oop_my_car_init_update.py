class Car:
    
    def __init__(self, n, c):
        self.name = n
        self.colour = c
    
    def start(self):
        print("Starting the engine")


my_car = Car('Nano', "yellow")

print(my_car.name)
print(my_car.colour)
my_car.start()
Car.start(my_car)  # using class method of any obj by the class