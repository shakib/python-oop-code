class Car:
    name = ""
    colour = ""
    
    def start():
        print("Starting the engine")
        
if __name__ == '__main__': #if use as a module or call from another file
    Car.name = 'Alion'
    Car.colour = 'black'
    print("Name of the Car:",Car.name)
    print("Colour:",Car.colour)
    Car.start()
    
    #print(dir(Car))

