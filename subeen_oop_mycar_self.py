class Car:
    def __init__(self, n, c):
        self.name = n
        self.colour = c
    
    def start(self):
        print("Starting the engine")
        print("colour: ", self.colour)
        print("name :",self.name)


my_car1 = Car('Nano', "yellow")
my_car1.start()

my_car2 = Car('Prado', "Silver")
my_car2.start()

my_car3 = Car('Pazero', "Black")
my_car3.start()