class Vehicle:
    """Base class for all vehicles and this quotations are called Docstring"""
    
    def __init__(self, Name, Manufacture, Color):
        self.name = Name
        self.manufacture = Manufacture
        self.color = Color
    
    def drive(self):
        print("Driving", self.manufacture, self.name)
    
    def turn(self, direction):
        print("Turning", self.name, "to", direction)
    
    def brake(self):
        print(self.name, "is stopping")


class car(Vehicle):  # ------------------ INHERITENCE ---------------------
    """Car class constructor """
    def __init__(self,name,manufacturer,color,year):
        self.name=name
        self.manufacture =manufacturer
        self.color = color
        self.year =year
        self.wheels =4
        print(" A new car has been created name:", self.name)
        print("It has",self.wheels,"wheels")
        print("The car was built in",self.year)
        
    
    def change_gear(self, gear_name):
        """Method for changing gear"""
        print(self.name, "is changing gear to", gear_name)


if __name__ == "__main__":
    c = car("Lancer Ex", "Mitsubishi", "Black",2017)