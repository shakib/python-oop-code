class Vehicle():
    def __init__(self,Name,Manufacturer,Color,Year,Cc):
        self.name = Name
        self.manufacturer = Manufacturer
        self.color = Color
        self.year = Year
        self.cc =Cc
        
    def start(self):
        print(self.name,' is started')
    def drive(self):
        print(self.name ,'is driving nice')
    def gear(self):
        print(self.name ,'is changing gear')
    def year(self):
        print('released in',self.year)
    def cc(self):
        print('engine power is ',self.cc)

car = Vehicle('Corolla','toyota','black','2002','125cc')
car.gear()
car.year()

bike = Vehicle('PULSSAR','YAMAHA','RED-BLACK','2009','100CC')
bike.gear()
bike.cc()